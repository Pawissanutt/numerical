
# 17
Approximate zeros within 10^(-6)


```python
tol = 1e-6
```


```python
def problem17_func(x=0): 
    try : 
        return 230*(x**4) + 18*(x**3) + 9*(x**2) - 221*x - 9
    except : 
        return None
```

# 17.a Method of False Position 


```python
def bisection(a, b, tol=tol, max_iteration=1000, function=problem17_func, print_result=True):
    FA = function(a)
    for i in range(max_iteration):
        r = a + (b-a)/2
        Fr = function(r)
        if Fr == 0 or (b-a)/2 < tol :
            if print_result : 
                print("Number of iterations :", i+1)
                print("Found root at x=", r)
            return r , i+1 
        if FA*Fr > 0 :
            a = r 
        else : 
            b = r 
    return None, i+1      
```


```python
def false_position(a,b, tol=tol, max_iteration=5000, function=problem17_func, print_result=True):
    p0, p1 = a, b
    q0 = function(a)
    q1 = function(b)
    for i in range(max_iteration):
        p = p1-q1*(p1-p0)/(q1-q0)
        if abs(p-p1) < tol : 
            if print_result : 
                print("Number of iterations :", i+1)
                print("Found root at x=", p)
            return p, i+1
        q = function(p)
        if q*q1 < 0 :
            p0 = p1
            q0 = q1 
        p1 = p 
        q1 = q 
    return None, i+1 
```


```python
print("#"*10+" Result "+"#"*10)
print("\nFinding root on range [-1,0]")
midpoints_neg, endpoint_neg = false_position(-1, 0,tol)
print("F(x) =", problem17_func(midpoints_neg))
print("\nFinding root on range [0,1]")
midpoints_pos, endpoint_pos = false_position(0, 1,tol)
print("F(x) =", problem17_func(midpoints_pos))
```

    ########## Result ##########
    
    Finding root on range [-1,0]
    Number of iterations : 16
    Found root at x= -0.04065849904334182
    F(x) = -0.0001749851898278365
    
    Finding root on range [0,1]
    Number of iterations : 8
    Found root at x= 0.9623983842387566
    F(x) = -2.29990515379086e-05
    

# 17.b Secant method


```python
def secant_formular(Pa,Pb, FPa, FPb):
    try : 
        return Pb - FPb*(Pb-Pa)/(FPb-FPa)
    except : 
        return None                                                                            
```


```python
def secant_method(a,b, tol=tol,max_iteration=5000, function=problem17_func):
    Pa = a
    Pb = b
    FPa = function(a)
    FPb = function(b)
    for i in range(max_iteration):
        Po = secant_formular(Pa,Pb, FPa, FPb)
        FPo = function(Po)
        if abs(Pa-Pb) < tol : 
            print("Number of iterations :", i+1)
            print("Found root at x=", Po)
            return Po , i+1
        Pa = Pb 
        FPa = FPb 
        Pb = Po
        FPb = FPo
    print("Failed to find root")
    return None, i+1
```


```python
print("#"*10+"Result"+"#"*10)
print("\nFinding root on range [-1,0]")
midpoints_neg, endpoint_neg = secant_method(-1,0,tol)
print("F(x) =", problem17_func(midpoints_neg))
print("\nFinding root on range [0,1]")
midpoints_pos, endpoint_pos = secant_method(0,1,tol)
print("F(x) =", problem17_func(midpoints_pos))
```

    ##########Result##########
    
    Finding root on range [-1,0]
    Number of iterations : 5
    Found root at x= -0.040659288315758865
    F(x) = 1.7763568394002505e-15
    
    Finding root on range [0,1]
    Number of iterations : 12
    Found root at x= -0.040659288315758865
    F(x) = 1.7763568394002505e-15
    

# 17.c Newton's method 


```python
def approx_fprime(Pa,Pb, FPa, FPb):
    try : 
        return (FPb - FPa)/(Pb-Pa)
    except : 
        return None 

def newton_formular(Pa, FPa, FPa_prime):
    try : 
        return Pa - FPa/FPa_prime
    except : 
        return None 
```


```python
def newton_method(a,b, tol=tol, max_frev=5000,function=problem17_func):
    P0 = a
    FP0 = function(P0)
    
    Pa = (a+b)/2
    FPa = function(Pa)
    FPa_prime = approx_fprime(P0,Pa, FP0, FPa)
    
    for i in range(max_frev):
        Po = newton_formular(Pa, FPa, FPa_prime)
        if abs(Po-Pa) < tol : 
            print("Number of iterations :", i+1)
            print("Found root at x=", Po)
            return Po , i+1
        P0 = Pa 
        Pa = Po
        FPa = function(Pa)
        FP0 = function(P0)
        FPa_prime = approx_fprime(P0, Pa, FP0, FPa)
    print("Failed to find root")
    return None, i+1
```


```python
print("#"*10+" Result "+"#"*10)
print("\nFinding root on range [-1,0]")
midpoints_neg, endpoint_neg = newton_method(-1,0,tol)
print("F(x) =", problem17_func(midpoints_neg))
print("\nFinding root on range [0,1]")
midpoints_pos, endpoint_pos = newton_method(0,1,tol)
print("F(x) =", problem17_func(midpoints_pos))
```

    ########## Result ##########
    
    Finding root on range [-1,0]
    Number of iterations : 6
    Found root at x= -0.040659288315759004
    F(x) = 3.197442310920451e-14
    
    Finding root on range [0,1]
    Number of iterations : 4
    Found root at x= -0.04065928835212746
    F(x) = 8.063079093290071e-09
    

# 18


```python
import numpy as np 

def problem18_func(x):
    try : 
        return np.sin(np.pi*x)
    except : 
        return None 
def case_a(a,b):
    return a+b < 2 

def case_b(a,b):
    return a+b > 2 

def case_c(a,b):
    return a+b == 2 
```


```python
range_a = np.linspace(-1,0, num=100)
range_b = np.linspace(2,3, num=100)

Pa, Pb, Pc = None, None, None 
Fa = 1
Fb = 1 
Fc = 1 

for a in range_a : 
    for b in range_b : 
        if case_a(a,b):
            r, it = bisection(a,b, function=problem18_func, print_result=False)
            Fr = problem18_func(r)
            if (Fr <= Fa or Fr == 0) and Fa != 0 : 
                Pa = r 
                Fa = Fr 
        if case_b(a,b):
            r, it = bisection(a,b, function=problem18_func, print_result=False)
            Fr = problem18_func(r)
            if (Fr <= Fb or Fr == 0) and Fb != 0 : 
                Pb = r 
                Fb = Fr
        if case_c(a,b):
            r, it = bisection(a,b, function=problem18_func, print_result=False)
            Fr = problem18_func(r)
            if (Fr <= Fc or Fr == 0) and Fc != 0: 
                Pc = r 
                Fc = Fr
                
print("a. a+b < 2")
print("Converges at x =", Pa)

print("b. a+b > 2")
print("Converges at x =", Pb)

print("c. a+b == 2")
print("Converges at x =", Pc)
```

    a. a+b < 2
    Converges at x = 0.0
    b. a+b > 2
    Converges at x = 1.9999990511422205
    c. a+b == 2
    Converges at x = 0.0
    

# 19a 

![title](img/19a.jpg)

# 19b

![title](img/19b.jpg)

# 19c

![title](img/19c.jpg)

# 27


```python
a = 135000
p = 1000
n = 360
```


```python
def oae_func(i):
    return  p /i *(1-(1+i)**(-n))
```


```python
def oae_zero_func(i):
    return (135*i)-1+((1+i)**(-360))
```


```python
x, it= bisection(a=0.000001,b=1,tol=1e-15,function=oae_zero_func)
```

    Number of iterations : 50
    Found root at x= 0.006749917159070622
    


```python
print("Checking result")
print("F(i) = ",oae_func(x) , " that should be nearly equal ", a)
```

    Checking result
    F(i) =  135000.0000000085  that should be nearly equal  135000
    


```python
# conclusion
print("maximal interest rate = ",x)
```

    maximal interest rate =  0.006749917159070622
    


```python

```


```python

```
